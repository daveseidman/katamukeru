﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacles : MonoBehaviour
{
    public int total = 3;
    public GameObject[] types;

    public void Generate() {
        for(int i = 0; i < total; i++) {
          var obstacle = Instantiate(types[0], new Vector2(Random.Range(-1f, 1f), i * 2), Quaternion.identity);
          obstacle.transform.parent = gameObject.transform;
        }
    }
}
