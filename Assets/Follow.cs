﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public GameObject objectToFollow;
    private Vector3 position;

    void Start() {
      position = transform.position;
    }

    void Update() {
      position.y = objectToFollow.transform.position.y;
      transform.position = position;
    }
}
