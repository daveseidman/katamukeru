﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class App : MonoBehaviour
{
    public GameObject startButton;
    public GameObject startScreen;
    public GameObject Obstacles;
    private Obstacles obstacles;
    public bool playing = false;

    // Start is called before the first frame update
    void Start()
    {
      obstacles = Obstacles.GetComponent<Obstacles>();
    }

    public void StartGame() {
      playing = true;
      startScreen.SetActive(false);
      obstacles.Generate();
    }

    public void EndGame() {
      startScreen.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
