﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spriteTest : MonoBehaviour
{
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
         anim.speed = 0f;
         // anim.Play("idle", 0, 0.25f);
    }

    // Update is called once per frame
    void Update()
    {
      float percent = Input.mousePosition.x / Screen.width;
      anim.Play("idle", 0, percent);
      // Debug.Log(percent);
    }
}
