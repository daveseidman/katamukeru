﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Cat : MonoBehaviour
{
    public GameObject game;
    private App app;
    private float minimumAngleForMovement = 0.5f;
    private class Bounds {
      public float min;
      public float max;
    }
    private Bounds bounds;
    private Vector2 position;
    private Vector2 speed;
    private float speedMax = 0.1f;
    private Vector2 acceleration = new Vector2(0.01f, 0.025f);
    private float dampen = 0.9f;
    private SpriteRenderer spriteRenderer;
    public Sprite[] IdleImages;
    private float idleIndex;
    private float idleSpeed = 0.1f;
    public Sprite Dead;
    public Sprite[] RollImages;
    public GameObject lifeMeter;
    private int lives = 9;

    void Start() {
      app = game.GetComponent<App>();
      Debug.Log(app.playing);

      bounds = new Bounds {
        // min = 0.0f,
        // max = Screen.width
        min = -2f,
        max = 2f
      };

      lifeMeter.GetComponent<Text>().text = "Lives: " + lives.ToString();

      position.x = 0;
      position.y = -2;

      spriteRenderer = GetComponent<SpriteRenderer>();
      // Camera cam = GetComponent<Camera>();
      float max = Camera.main.orthographicSize * Screen.width / Screen.height;
    }


    private void updatePosition() {
      // orientation
      if(Mathf.Abs(Mathf.Rad2Deg * Input.acceleration.x) >= minimumAngleForMovement) {
        // Debug.Log("here");
        position.x += Input.acceleration.x;
      }

      // keyboard
      // if(Input.GetKey("up")) {
      //   speed.y += 0.01f;
      // }
      // if(Input.GetKey("down")) {
      //   speed.y -= 0.01f;
      // }
      if(Input.GetKey("left")) {
        speed.x -= acceleration.x;
      }
      if(Input.GetKey("right")) {
        speed.x += acceleration.x;
      }

      speed.x *= dampen;
      speed.x = Mathf.Clamp(speed.x, -speedMax, speedMax);
      // speed.y = Mathf.Clamp(speed.y, -speedMax, speedMax);
      // positiosn.x += speed.x;

      position.y += acceleration.y;
      position += speed;
      position.x = Mathf.Clamp(position.x, bounds.min, bounds.max);

      transform.position = position;
    }

    private void updateSprite() {
      // TODO: should be clock based:
      idleIndex += idleSpeed;
      if(idleIndex >= IdleImages.Length) idleIndex = 0;

      float percent = (speed.x + speedMax) / (speedMax * 2);
      int index = Mathf.RoundToInt(percent * (RollImages.Length - 1));

      // centered
      if(index == Mathf.RoundToInt(RollImages.Length / 2)) {
        spriteRenderer.sprite = IdleImages[Mathf.FloorToInt(idleIndex)];
      }
      // rolling
      else {
        spriteRenderer.sprite = RollImages[index];
      }
    }

    void Update() {
      updatePosition();
      updateSprite();
    }

    void OnTriggerExit2D(Collider2D col2)
    {
      spriteRenderer.sprite = IdleImages[Mathf.FloorToInt(idleIndex)];
    }

    void OnTriggerEnter2D(Collider2D col) {
      spriteRenderer.sprite = Dead;
      lives--;
      lifeMeter.GetComponent<Text>().text = "Lives: " + lives.ToString();
      if(lives == 0) {
        app.EndGame();
      }
    }
}
