﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftOrRightPole : MonoBehaviour
{
    public GameObject left;
    public GameObject right;

    void Start() {
        bool leftActive = Random.Range(0f, 1f) > 0.5f;
        left.SetActive(leftActive);
        right.SetActive(!leftActive);
    }

}
