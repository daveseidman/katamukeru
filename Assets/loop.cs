﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class loop : MonoBehaviour
{
  private float frame = 0.0f;
  public float frames = 5f;

    IEnumerator Loop() {
      while(true) {
        frame += (1/frames);
        if(frame >= 1f) {
          frame = 0.0f;
        }
        Debug.Log(frame);
        yield return new WaitForSeconds(.025f);
      }
    }

    void Start()
    {
        StartCoroutine("Loop");
    }
}
